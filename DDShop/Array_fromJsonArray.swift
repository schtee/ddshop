//
//  JSONSerializableArray.swift
//  DDShop
//
//  Created by Steve Taylor on 16/01/2017.
//  Copyright © 2017 Steve Taylor. All rights reserved.
//

extension Array where Element: JSONSerializable {
    init(fromJsonArray jsonArray:[Any]) {
        var elements = [Element]()
        
        for obj in jsonArray {
            guard let jsonDict = obj as? [String:Any] else {
                continue
            }
            
            let instance = Element(fromJsonDict: jsonDict)
            elements.append(instance)
        }
        
        self.init(elements)
    }
}
