//
//  BasketController.swift
//  DDShop
//
//  Created by Steve Taylor on 18/01/2017.
//  Copyright © 2017 Steve Taylor. All rights reserved.
//

import Foundation

class BasketController {
    
    private static let kCartUrl = URL(string: "https://private-anon-1acfdec014-ddshop.apiary-mock.com/cart")!
    
    private let basket: ShopModel.Basket
    
    var productRecords:[ProductRecord] {
        get {
            return Array(basket.products.values)
        }
    }
    
    init(_ basket: ShopModel.Basket) {
        self.basket = basket
    }
    
    func addProduct(_ product: Product, _ successHandler: @escaping () -> Void, _ failureHandler: @escaping () -> Void) {
        let jsonDict = [ [ "productId" : product.productId ] ]
        var urlRequest = URLRequest(url: BasketController.kCartUrl)
        urlRequest.httpMethod = HttpRequest.kPost
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: jsonDict) else {
            return
        }
        urlRequest.httpBody = jsonData
        
        let request = HttpRequest(with: urlRequest, successHandler: { data in
            self.handleProductAdded(product, data)
            successHandler()
        },  failureHandler: { error in
            self.handleError(error)
            failureHandler()
        })
        request.start()
    }
    
    private func handleProductAdded(_ product: Product, _ data: Data?) {
        guard let data = data?.toJSON() else {
            handleError(HttpRequest.HttpRequestError.NoData)
            return
        }
        
        guard let jsonDict = data as? [String:Any] else {
            handleError(HttpRequest.HttpRequestError.IncorrectDataFormat)
            return
        }
        
        // Mock endpoint will only ever return product id 123 - use one client requested
        if var cartId = jsonDict["cartId"] as? Int {
            // Mock endpoint will cause collisions on cartId key in products dictionary
            // This is a minimally invasive hack to work around this
            if basket.products.keys.contains(cartId) {
                cartId = basket.products.keys.sorted().last! + 1
            }
            basket.products[cartId] = ShopModel.Basket.BasketRecord(cartId: cartId, product: product)
        }
    }
    
    private func handleError(_ error: Error) {
        print(error)
    }
    
    func removeProduct(_ id: Int, _ successHandler: @escaping ([ProductRecord]) -> Void, _ failureHandler: @escaping () -> Void) {
        let url = BasketController.kCartUrl.appendingPathComponent(String(id))
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = HttpRequest.kDelete
        
        let request = HttpRequest(with: urlRequest, successHandler: { data in
            self.handleProductRemoved(id)
            successHandler(self.productRecords)
        },  failureHandler: { error in
            self.handleError(error)
            failureHandler()
        })
        request.start()
    }
    
    private func handleProductRemoved(_ id: Int) {
        basket.products.removeValue(forKey: id)
    }
}
