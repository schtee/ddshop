//
//  CatalogueController.swift
//  DDShop
//
//  Created by Steve Taylor on 18/01/2017.
//  Copyright © 2017 Steve Taylor. All rights reserved.
//

import Foundation

class CatalogueController {
    
    private static let kProductsUrl = URL(string:"https://private-anon-1acfdec014-ddshop.apiary-mock.com/products")!
    
    private let catalogue:ShopModel.Catalogue
    
    init(_ catalogue:ShopModel.Catalogue) {
        self.catalogue = catalogue
    }
    
    func requestAllProducts(_ callback:@escaping (ShopModel.Catalogue) -> Void) {
        let request = HttpRequest(CatalogueController.kProductsUrl, { data in
            guard let data = data else {
                self.handleError(HttpRequest.HttpRequestError.NoData)
                return
            }
            self.populateCatalogue(data: data)
            callback(self.catalogue)

        }, self.handleError)
        request.start()
    }
    
    private func populateCatalogue(data:Data) {
        guard let jsonArray = data.toJSON() as? [Any] else {
            handleError(HttpRequest.HttpRequestError.IncorrectDataFormat)
            return
        }
        
        catalogue.allProducts = Array<Product>.init(fromJsonArray: jsonArray)
        setupCategories()
    }
    
    private func setupCategories() {
        catalogue.categories = [:]
        for product in catalogue.allProducts {
            var category = catalogue.categories[product.category] ?? [Product]()
            category.append(product)
            catalogue.categories[product.category] = category
        }
        catalogue.categoryNames = catalogue.categories.keys.sorted()
    }
    
    private func handleError(_ error:Error) {
        print(error)
    }
}
