//
//  JSONHttpRequest.swift
//  DDShop
//
//  Created by Steve Taylor on 15/01/2017.
//  Copyright © 2017 Steve Taylor. All rights reserved.
//

import UIKit

class HttpRequest {
    static let kPost = "POST"
    static let kDelete = "DELETE"
    
    enum HttpRequestError : Error {
        case NoData
        case IncorrectDataFormat
    }
    
    private let request:URLRequest
    private let successHandler:(Data?) -> Void
    private let failureHandler:(Error) -> Void
    
    init(_ url:URL, _ successHandler:@escaping (Data?) -> Void, _ failureHandler:@escaping (Error) -> Void) {
        self.request = URLRequest(url: url)
        self.successHandler = successHandler
        self.failureHandler = failureHandler
    }
    
    init(with request:URLRequest, successHandler:@escaping (Data?) -> Void, failureHandler:@escaping (Error) -> Void) {
        self.request = request
        self.successHandler = successHandler
        self.failureHandler = failureHandler
    }
    
    func start() {
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                DispatchQueue.main.async {
                    self.failureHandler(error!)
                }
                return
            }
            
            DispatchQueue.main.async {
                self.successHandler(data)
            }
        }
        task.resume()
    }
}
