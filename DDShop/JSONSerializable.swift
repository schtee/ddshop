//
//  JSONSerializable.swift
//  DDShop
//
//  Created by Steve Taylor on 16/01/2017.
//  Copyright © 2017 Steve Taylor. All rights reserved.
//

import UIKit

protocol JSONSerializable {
    init(fromJsonDict:[String:Any])
}
