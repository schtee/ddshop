//
//  NavBarViewController.swift
//  DDShop
//
//  Created by Steve Taylor on 15/01/2017.
//  Copyright © 2017 Steve Taylor. All rights reserved.
//

import UIKit

class NavBarViewController: UITableViewController {
    @IBOutlet var spinner: UIActivityIndicatorView?
    
    private let model = ShopModel()
    
    private var catalogueController: CatalogueController!
    private var basketController: BasketController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.catalogueController = CatalogueController(model.catalogue)
        self.basketController = BasketController(model.basket)

        if let split = splitViewController {
            split.preferredDisplayMode = UISplitViewControllerDisplayMode.allVisible
            let controllers = split.viewControllers
            let detailNavigationController = controllers[controllers.count-1] as! UINavigationController
            let basketButton = UIBarButtonItem(title: "Basket", style: UIBarButtonItemStyle.plain, target: self, action: #selector(NavBarViewController.handleBasketButtonPressed))
            detailNavigationController.navigationItem.rightBarButtonItem = basketButton
        }
        
        catalogueController?.requestAllProducts(self.populateProducts)
    }
    
    func handleBasketButtonPressed() {
        print("Basket button pressed")
    }
    
    func populateProducts(catalogue: ShopModel.Catalogue) {
        print("Products ready")
        DispatchQueue.main.async {
            if let view = self.view as? UITableView {
                view.reloadData()
            }
            self.spinner?.isHidden = true
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.clearsSelectionOnViewWillAppear = self.splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let categoryName = model.catalogue.categoryNames[indexPath.row]
                let products = model.catalogue.categories[categoryName]
                let controller = (segue.destination as! UINavigationController).topViewController as! ProductsViewController
                
                controller.setup(with: products!, basketController: basketController)
                controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.catalogue.categoryNames.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let category = model.catalogue.categoryNames[indexPath.row]
        cell.textLabel!.text = category
        return cell
    }
}

