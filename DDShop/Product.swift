//
//  Product.swift
//  DDShop
//
//  Created by Steve Taylor on 15/01/2017.
//  Copyright © 2017 Steve Taylor. All rights reserved.
//

protocol ProductRecord {
    var id: Int {
        get
    }
    
    var product: Product {
        get
    }
}

class Product: JSONSerializable, ProductRecord {
    var id: Int {
        get { return productId }
    }
    
    var product:Product {
        get { return self }
    }
    
    var productId: Int = 0
    var name: String = ""
    var category: String = ""
    var price: Double = 0.0
    var oldPrice: Double?
    var stock: Int = 0
    
    private static let currencyFormatString = "£%.2f"
    
    // Server doesn't offer multicurrency
    var priceString: String {
        get {
            return priceToPriceString(price: price)
        }
    }
    
    var oldPriceString: String? {
        get {
            if let oldPrice = oldPrice {
                return priceToPriceString(price: oldPrice)
            }
            else {
                return nil
            }
        }
    }
    
    private func priceToPriceString(price:Double) -> String {
        return String(format: Product.currencyFormatString, arguments: [price] )
    }
    
    init(_ productId:Int, _ name:String, _ category:String, _ price:Double, _ oldPrice:Double?, _ stock:Int) {
        self.productId = productId
        self.name = name
        self.category = category
        self.price = price
        self.oldPrice = oldPrice
        self.stock = stock
    }
    
    required init(fromJsonDict:[String:Any]) {
        self.productId = fromJsonDict["productId"] as! Int
        self.name = fromJsonDict["name"] as! String
        self.category = fromJsonDict["category"] as! String
        self.price = fromJsonDict["price"] as! Double
        self.oldPrice = fromJsonDict["oldPrice"] as? Double
        self.stock = fromJsonDict["stock"] as! Int
    }
}
