//
//  ProductCollectionViewCell.swift
//  DDShop
//
//  Created by Steve Taylor on 15/01/2017.
//  Copyright © 2017 Steve Taylor. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak private var image: UIImageView!
    @IBOutlet weak private var nameLabel: UILabel!
    @IBOutlet weak private var priceLabel: UILabel!
    @IBOutlet weak private var oldPriceLabel: UILabel!
    @IBOutlet weak private var stockLabel: UILabel!
    
    @IBOutlet weak private var addToBasketButton: UIButton!
    @IBOutlet weak private var removeFromBasketButton: UIButton!
    
    private var productRecord:ProductRecord!
    private var clickHandler:((ProductRecord, UIView) -> Void)! = nil
    
    func setup(withProductRecord productRecord: ProductRecord, mode:ProductsViewController.Mode, clickHandler: @escaping (ProductRecord, UIView) -> Void) {
        self.productRecord = productRecord
        self.clickHandler = clickHandler
        setupButtonForMode(mode)
        
        nameLabel?.text = productRecord.product.name
        
        if let oldPriceString = productRecord.product.oldPriceString {
            oldPriceLabel?.text = "Was: " + oldPriceString
            priceLabel?.textColor = UIColor.red
            priceLabel?.text = "Now: " + productRecord.product.priceString
        }
        else {
            oldPriceLabel?.isHidden = true
            priceLabel?.textColor = UIColor.black
            priceLabel?.text = productRecord.product.priceString
        }
        stockLabel?.text = String(describing: productRecord.product.stock) + " in stock"
    }
    
    private func setupButtonForMode(_ mode:ProductsViewController.Mode) {
        let buttonToHide = mode == ProductsViewController.Mode.Basket ? addToBasketButton : removeFromBasketButton
        let buttonToShow = mode == ProductsViewController.Mode.Basket ? removeFromBasketButton : addToBasketButton
        buttonToHide?.isHidden = true
        buttonToShow?.isHidden = false
    }
    
    @IBAction func onClick(sender:UIView) {
        clickHandler(productRecord, sender)
    }
}
