//
//  ProductViewController.swift
//  DDShop
//
//  Created by Steve Taylor on 15/01/2017.
//  Copyright © 2017 Steve Taylor. All rights reserved.
//

import UIKit

class ProductsViewController : UICollectionViewController {
    enum Mode {
        case Catalogue
        case Basket
    }
    
    private static let kShowBasketSegue = "Show Basket"
    
    @IBOutlet private var productsView: UICollectionView!
    @IBOutlet private var basketButton: UIBarButtonItem!
    
    private var model: ShopModel?
    private(set) var productRecords: [ProductRecord]? {
        didSet {
            collectionView?.reloadData()
        }
    }
    private var basketController: BasketController!
    private var mode: Mode = Mode.Catalogue
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if productRecords == nil {
            productsView.backgroundColor = UIColor.lightGray
        }
        else {
            productsView.backgroundColor = UIColor.white
        }
    }

    
    func setup(with productRecords: [ProductRecord], basketController: BasketController, mode: Mode = Mode.Catalogue) {
        self.productRecords = productRecords
        self.basketController = basketController
        self.mode = mode
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productRecords?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath)
        if let productCell = cell as? ProductCollectionViewCell {
            guard let productRecord = productRecords?[indexPath.row] else {
                return cell
            }

            productCell.setup(withProductRecord: productRecord, mode:mode, clickHandler: handleBasketButtonClick)
        }
        return cell
    }

    private func handleBasketButtonClick(productRecord: ProductRecord, button: UIView) {
        let spinner = addTemporarySpinnerToView(button)
        if mode == Mode.Catalogue {
            let callback = {
                self.removeTemporarySpinner(view: button, spinner: spinner)
            }
            addProductToBasket(productRecord, callback)
        }
        else {
            let callback: ([ProductRecord]?) -> Void = {
                self.removeTemporarySpinner(view: button, spinner: spinner)
                
                guard let productRecords = $0 else {
                    return
                }
                
                self.productRecords = productRecords
            }
            removeProductFromBasket(productRecord, callback)
        }
    }
    
    private func addTemporarySpinnerToView(_ view: UIView) -> UIActivityIndicatorView {
        view.isUserInteractionEnabled = false
        view.tintColor = UIColor.lightGray
        
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        spinner.center = view.center
        spinner.startAnimating()
        view.superview?.insertSubview(spinner, aboveSubview: view)
        
        return spinner
    }
    
    private func removeTemporarySpinner(view:UIView, spinner: UIActivityIndicatorView) {
        view.tintColor = nil
        view.isUserInteractionEnabled = true
        
        spinner.removeFromSuperview()
    }
    
    private func addProductToBasket(_ productRecord: ProductRecord, _ callback: @escaping (() -> Void)) {
        basketController.addProduct(productRecord.product, callback, callback)
    }
    
    private func removeProductFromBasket(_ productRecord: ProductRecord, _ callback: @escaping ([ProductRecord]?) -> Void) {
        basketController.removeProduct(productRecord.id, callback, {
            callback(nil)
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == ProductsViewController.kShowBasketSegue) {
            guard let basketView = segue.destination as? ProductsViewController else {
                return
            }
            
            basketView.basketButton.tintColor = UIColor.clear
            basketView.basketButton.isEnabled = false
            
            basketView.setup(with: basketController.productRecords, basketController: basketController, mode:Mode.Basket)
        }
    }
}

