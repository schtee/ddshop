//
//  ShopModel.swift
//  DDShop
//
//  Created by Steve Taylor on 17/01/2017.
//  Copyright © 2017 Steve Taylor. All rights reserved.
//

class ShopModel {
    struct StandardProductRecord {
        let product: Product
        var id: Int {
            get { return product.productId }
        }
    }
    
    class Catalogue {
        var allProducts = [Product]()
        var categories = [String:[Product]]()
        var categoryNames = [String]()
    }
    
    class Basket {
        struct BasketRecord : ProductRecord {
            let cartId: Int
            let product: Product
            
            var id: Int {
                get {
                    return cartId
                }
            }
        }
        
        var products = [Int:ProductRecord]()
    }
    
    var catalogue = Catalogue()
    var basket = Basket()
}
